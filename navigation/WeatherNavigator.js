import React from 'react'
import { Text, View, Image, TouchableOpacity, StyleSheet } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Icon from 'react-native-vector-icons/FontAwesome';

import { breadcrumbScreen } from '../src/constants'

const WeatherMainStackNavigator = createStackNavigator()
const WeatherRootStackNavigator = createStackNavigator()

const BottomTab = createBottomTabNavigator()

const WeatherMainNavigator = () => {
    return <WeatherMainStackNavigator.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: '#01bbb3',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }}
    >
        <WeatherMainStackNavigator.Screen
            name={breadcrumbScreen.citiesWeather.name}
            component={breadcrumbScreen.citiesWeather.component}
            options={({ route }) => ({ title: breadcrumbScreen.citiesWeather.title })}
        />
        <WeatherMainStackNavigator.Screen
            name={breadcrumbScreen.cityWeatherDetail.name}
            component={breadcrumbScreen.cityWeatherDetail.component}
            options={({ route }) => ({ title: route.params.title })} />
    </WeatherMainStackNavigator.Navigator>
}

const WeahterCustomBottomTab = (props) => {
    const { children, onPress } = props
    const ifFocused = props.accessibilityState.selected

    return <TouchableOpacity
        style={{
            top: -30,
            justifyContent: 'center',
            alignItems: 'center',
            ...styles.shadow
        }}
        onPress={onPress}
    >
        <View
            style={{
                width: 70,
                height: 70,
                borderRadius: 35,
                backgroundColor: ifFocused ? '#ec6e4c' : '#748c94',
                ...styles.shadow
            }}
        >
            {children}
        </View>
    </TouchableOpacity>
}

export const WeatherBottomTab = () => {
    return <BottomTab.Navigator
        tabBarOptions={{
            showLabel: false,
            style: {
                position: 'absolute',
                bottom: 25,
                left: 20,
                right: 20,
                elevation: 0,
                backgroundColor: '#ffffff',
                borderRadius: 15,
                height: 90,
                ...styles.shadow
            }
        }}
    >
        <BottomTab.Screen
            name={breadcrumbScreen.citiesWeather.name}
            component={WeatherMainNavigator}
            options={{
                tabBarIcon: ({ focused }) => (
                    <View style={{ alignItems: 'center', justifyContent: 'center', top: 10 }}>
                        <Image
                            source={require('../assets/icons/home.png')}
                            resizeMode='contain'
                            style={{
                                width: 25,
                                height: 25,
                                tintColor: focused ? '#ec6e4c' : '#748c94'
                            }} />
                        <Text
                            style={{ color: focused ? '#ec6e4c' : '#748c94', fontSize: 12 }}
                        >HOME</Text>
                    </View>
                ),
            }}
        />
        <BottomTab.Screen
            name={breadcrumbScreen.searchCity.name}
            component={breadcrumbScreen.searchCity.component}
            options={{
                tabBarIcon: ({ focused }) => (
                    <>
                        <Image
                            source={require('../assets/icons/search.png')}
                            resizeMode="contain"
                            style={{
                                width: 30,
                                height: 30,
                                tintColor: '#fff'
                            }}
                        />
                        <Text
                            style={{ color: '#fff', fontSize: 12 }}
                        >SEARCH</Text>
                    </>
                ),
                tabBarButton: (props) => (
                    <WeahterCustomBottomTab {...props} />
                )
            }}
        />
        <BottomTab.Screen
            name={breadcrumbScreen.favoritesCitiesWeather.name}
            component={breadcrumbScreen.favoritesCitiesWeather.component}
            options={{
                tabBarIcon: ({ focused }) => (
                    <View style={{ alignItems: 'center', justifyContent: 'center', top: 10 }}>
                        <Image
                            source={require('../assets/icons/bookmark.png')}
                            resizeMode='contain'
                            style={{
                                width: 25,
                                height: 25,
                                tintColor: focused ? '#ec6e4c' : '#748c94'
                            }} />
                        <Text
                            style={{ color: focused ? '#ec6e4c' : '#748c94', fontSize: 12 }}
                        >FAVORITES</Text>
                    </View>
                ),
            }}
        />
    </BottomTab.Navigator>
}

export const WeatherRootNavigator = () => {
    return <WeatherRootStackNavigator.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: '#01bbb3',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }}>
        <WeatherRootStackNavigator.Screen
            name={breadcrumbScreen.initWeather.name}
            component={breadcrumbScreen.initWeather.component}
            options={({ route }) => ({ title: breadcrumbScreen.initWeather.title })} />
        <WeatherRootStackNavigator.Screen options={{ headerShown: false }}
            name="WeatherBottomTabs"
            component={WeatherBottomTab}
        />
    </WeatherRootStackNavigator.Navigator>
}

const styles = StyleSheet.create({
    shadow: {
        shadowColor: '#7F5DF0',
        shadowOffset: {
            width: 0,
            height: 10
        },
        shadowOpacity: 0.50,
        shadowRadius: 4,
        elevation: 6
    }
})