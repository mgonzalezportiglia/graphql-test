import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { WeatherRootNavigator } from './WeatherNavigator'

const WeatherAppNavigator = props => {
    return <NavigationContainer>
        <WeatherRootNavigator />
    </NavigationContainer>;
}

export default WeatherAppNavigator