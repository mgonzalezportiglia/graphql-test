import React, { useState, useCallback } from 'react'
import { View, Text, Button, SafeAreaView, TextInput, Keyboard, Alert, TouchableOpacity, StyleSheet, Pressable } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { gql, useLazyQuery } from '@apollo/client'
import { useFocusEffect } from '@react-navigation/native'
import { useStore } from '../../store/storeHook'
import styled from 'styled-components/native';

const Container = styled.SafeAreaView`
    flex: 1;
    justify-content: center;
    align-items: center;
`

const ContainerForm = styled.View`
    background-color: #ec6e4c;
    border-radius: 10px;
    padding: 15px;
`

const TextStyled = styled.Text`
    font-size: 16px;
    font-weight: bold;
    color: #fcfefe;
    width: 200px;
    text-align: center;
`

const TextInputStyled = styled.TextInput`
    background-color: #ffffff;
    border-radius: 5px;
    margin: 20px 0px;
    text-align: center;
`

const GET_CITY_BY_NAME_QUERY = gql`
    query($cityName: String!) {
        getCityByName(name: $cityName) {
            id
            name
            weather {
                summary {
                    icon
                    description
                }
                temperature {
                    actual
                    min
                    max
                }
            }
            coord {
                lon
                lat
            }
        }
    }
`

const saveDataIntoAsync = async (newValue) => {
    try {
        const storedValue = await getDataFromAsync()
        let updatedAsyncStored;
        if (storedValue !== null) {
            updatedAsyncStored = storedValue !== undefined ? storedValue + "," + JSON.stringify(newValue) : JSON.stringify(newValue)

        } else {
            updatedAsyncStored = JSON.stringify(value)
        }
        await AsyncStorage.setItem('@storage_search_city', updatedAsyncStored)
    } catch (e) {
        console.log(e)
    }
}

const getDataFromAsync = async () => {
    try {
        const value = await AsyncStorage.getItem('@storage_search_city')
        if (value !== null) {
            return value
        }
    } catch (error) {
        console.log(error)
    }
}

const SearchCityModalScreen = (props) => {

    const [store, setStore] = useStore()

    useFocusEffect(
        useCallback(() => {
            AsyncStorage.clear()
            return async () => {
                const storedValue = await getDataFromAsync()
                // save async storage to global state before leave page.
                if (storedValue !== null && storedValue !== undefined) {
                    const storedValueParsed = JSON.parse(storedValue)
                    const updatedCitiesStore = [...store.cities, storedValueParsed]
                    setStore('SET_CITIES', updatedCitiesStore)
                }

            };
        }, [])
    )

    const showAlert = (city: any) => {
        Alert.alert(
            'Alert',
            'Do you want to add the location?',
            [
                {
                    text: 'Cancel',
                    style: 'cancel'
                },
                {
                    text: 'Ok',
                    onPress: () => {
                        saveDataIntoAsync(city)
                    }
                }
            ]
        )
    }

    const showAlertError = () => {
        Alert.alert(
            'Error',
            'The city is already added.',
            [
                {
                    text: 'Ok',
                    style: 'cancel'
                }
            ]
        )
    }

    //esta funcion se ejecuta al momento que yo deseo(es decir otro tipo de eventos) no como useQuery.
    const [getCityByNameLazy, { loading, data }] = useLazyQuery(GET_CITY_BY_NAME_QUERY, {
        onCompleted: () => {
            setInputText('')
            const city = data.getCityByName
            const alreadyExist = store.cities.findIndex((c) => c.id === city.id)
            if (alreadyExist !== -1) {
                showAlertError()
            } else {
                showAlert(city)
            }
        }
    })

    const [inputText, setInputText] = useState()

    const submitSearchHandler = () => {
        Keyboard.dismiss()
        getCityByNameLazy({
            variables: {
                cityName: inputText
            }
        })

    }

    const CustomButton = ({ onPress, title }) => (
        <Pressable
            style={styles.customButtonContainer}
            onPress={onPress}
            title="Search" >
            <Text style={styles.customButtonText}>{title}</Text>
        </Pressable>
    );

    return (
        <Container>
            <ContainerForm>
                <TextStyled>Search the location by name</TextStyled>
                <TextInputStyled
                    onChangeText={setInputText}
                    value={inputText}
                />

                <CustomButton
                    onPress={submitSearchHandler}
                    title="Search" />
            </ContainerForm>
        </Container>
    )


}

const styles = StyleSheet.create({
    customButtonContainer: {
        borderRadius: 5,
        backgroundColor: '#fcfefe',
        height: 40,
        justifyContent: 'center'
    },
    customButtonText: {
        color: '#ec6e4c',
        textAlign: 'center',
        fontWeight: 'bold',
        textTransform: 'uppercase'
    }
})

export default SearchCityModalScreen