import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Button, Image, Animated, Easing } from 'react-native'
import { useStore } from '../store/storeHook'
import { formatTemperature } from '../utils/commonMedhods'
import MapView, { Marker } from 'react-native-maps'

const CityWeatherDetailScreen = props => {

    const spinLogo = new Animated.Value(0)

    Animated.loop(
        Animated.timing(
            spinLogo,
            {
                toValue: 1,
                duration: 10000,
                easing: Easing.linear,
                useNativeDriver: true
            }
        )
    ).start()

    const spin = spinLogo.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
    })

    const { cityId } = props.route.params
    const [store, setStore] = useStore()
    const [state, setState] = useState(undefined)

    useEffect(() => {
        const updatedState = store.cities.find(city => city.id === cityId)
        setState(updatedState)
    }, [])

    const actualDate = new Date()

    if (state !== undefined) {
        return (
            <View style={styles.container}>
                <View style={styles.topInfo}>
                    <View>
                        <Text style={{ fontSize: 60, color: '#fcfefe' }}>{formatTemperature(state.weather.temperature.actual)}</Text>
                    </View>
                    <View>
                        <View style={styles.topInfoRight}>
                            <View style={{ flexDirection: 'row' }}>
                                <View>
                                    <Image style={styles.iconStyle} source={{ uri: `http://openweathermap.org/img/wn/${state.weather.summary.icon}@2x.png` }} />
                                </View>
                                <View style={{ top: 15 }}>
                                    <Text style={[styles.commonFontTemp, { color: '#fcfefe' }]}>{formatTemperature(state.weather.temperature.min)}.</Text>
                                    <Text style={[styles.commonFontTemp, { color: '#fda82f' }]}>{formatTemperature(state.weather.temperature.max)}.</Text>
                                </View>
                            </View>
                            <View>
                                <View>
                                    <Text style={styles.commonFont}>{state.weather.summary.description}</Text>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.commonFont}>{actualDate.toDateString()}</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 1, alignItems: 'center', top: -150 }}>
                    <View style={{ borderRadius: 15 }}>
                        <MapView
                            style={styles.map}
                            initialRegion={{
                                latitude: state.coord.lat,
                                longitude: state.coord.lon,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }}>
                            <Marker
                                coordinate={{ latitude: state.coord.lat, longitude: state.coord.lon }}
                                title={state.name}
                            >
                                <Animated.Image
                                    source={require('../assets/sun.png')}
                                    style={{ transform: [{ rotate: spin }], width: 60, height: 60 }}
                                />
                            </Marker>
                        </MapView>
                    </View>
                </View>
                <View style={{ bottom: 120 }}>
                    <Text style={styles.commonFont}>{actualDate.toLocaleTimeString().substring(0, 5)}</Text>
                </View>
            </View>
        )
    }

    return (
        <View>
            <Text>Loading...</Text>
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3ec1c9',
        paddingTop: 20,
        paddingBottom: 20
    },
    topInfo: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    iconStyle: {
        width: 80,
        height: 80
    },
    topInfoRight: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch'
    },
    commonFont: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    commonFontTemp: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    map: {
        width: 300,
        height: 300
    }
})

export default CityWeatherDetailScreen