import React, { useState, useCallback } from 'react'
import { View, Text, StyleSheet, FlatList, ScrollView, TouchableOpacity, Button, Image } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { useStore } from '../store/storeHook'
import { useFocusEffect } from '@react-navigation/native'

import Card from '../components/Card'
import { addToFavHandler, isFavorite } from '../utils/commonMedhods'
import { formatTemperature } from '../utils/commonMedhods'

const CitiesWeatherScreen = props => {

    const [store, setStore] = useStore()
    const [filteredState, setFilteredState] = useState([])
    const [filterActive, setFilterActive] = useState('all')
    const [filterWeather, setFilterWeather] = useState(['all'])

    useFocusEffect(
        useCallback(() => {
            setFilteredState(store.cities)
            const dinamicFilter = store.cities.map((item) => item.weather.summary.icon)
                .filter((a, b, array) => array.indexOf(a) === b)

            setFilterWeather(['all'].concat(dinamicFilter))

        }, [store.cities])
    )

    const { navigation } = props

    const navigationHandler = (screen: string, id: string, name: string) => {
        navigation.navigate(screen, {
            cityId: id,
            title: name
        })
    }

    const onAddToFav = (id: string) => {
        const updatedStore = addToFavHandler(id, store)
        setStore('SET_FAVORITES', updatedStore)
    }

    const onPressFilterHandler = (filter: string) => {
        let filteredCities = []

        if (filter === 'all') {
            filteredCities = store.cities
        } else {
            filteredCities = store.cities.filter(({ weather }) => weather.summary.icon === filter)
        }

        setFilterActive(filter)
        setFilteredState(filteredCities)

    }

    const renderGridItem = (itemData) => {
        const { weather } = itemData.item

        return <Card
            navigation={navigationHandler.bind(this, 'CityWeatherDetail', itemData.item.id, itemData.item.name)}
            style={styles.cardCustomStyle}>

            <View style={styles.cardViewContainer}>
                <View>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#0e0e0e' }}>{itemData.item.name}</Text>
                    <Image style={styles.iconStyle} source={{ uri: `http://openweathermap.org/img/wn/${weather.summary.icon}@2x.png` }} />
                </View>
                <View style={styles.cardViewCol}>
                    <View style={styles.temperatureContent}>
                        <Text style={{ fontSize: 50, color: '#fcfefe' }}>{formatTemperature(weather.temperature.actual)}</Text>
                    </View>
                </View>
                <View>
                    <TouchableOpacity style={{ top: -50 }} onPress={onAddToFav.bind(this, itemData.item.id)}>
                        <View>
                            <Icon
                                name="bookmark"
                                size={25}
                                color={isFavorite(itemData.item.id, store.favorites) ? '#fda82f' : '#fff'}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>

        </Card>
    }

    return (
        <View style={styles.mainContainer}>
            <View style={styles.containerHorizontalScroll}>
                <ScrollView
                    horizontal={true}>
                    {
                        filterWeather.map(weather => (
                            <TouchableOpacity
                                key={weather}
                                onPress={onPressFilterHandler.bind(this, weather)}>
                                <View style={[styles.filterButton, filterActive === weather ? { backgroundColor: '#3ec1c9', borderColor: '#3ec1c9' } : {}]}>
                                    {
                                        weather === 'all' ?
                                            <Text></Text>
                                            :
                                            <Image
                                                style={styles.iconFilterStyle}
                                                source={{ uri: `http://openweathermap.org/img/wn/${weather}@2x.png` }}
                                            />
                                    }
                                </View>
                                <View>
                                    <Text style={[filterActive === weather ? { fontSize: 12 } : { fontSize: 10 }, { textTransform: 'capitalize', fontWeight: 'bold', color: '#86c4b5', maxWidth: 60, textAlign: 'center' }]}>{weather}</Text>
                                </View>
                            </TouchableOpacity>
                        ))
                    }
                </ScrollView>
            </View>

            <View style={{ flex: 1 }}>
                {
                    filteredState.length > 0 ?
                        <FlatList
                            keyExtractor={(item, index) => item.id}
                            numColumns={1}
                            data={filteredState}
                            renderItem={renderGridItem}
                        />

                        :
                        <View style={{ top: 100, alignItems: 'center' }}>
                            <Text style={{
                                fontSize: 18,
                                fontWeight: 'bold',
                                color: '#ec6e4c'
                            }}>There is no city finded.</Text>
                        </View>
                }
            </View>

        </View >
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    containerHorizontalScroll: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 12,
        paddingRight: 12
    },
    filterButton: {
        backgroundColor: '#86c4b5',
        marginRight: 15,
        padding: 5,
        width: 60,
        height: 60,
        borderRadius: 60,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#7fbaac',
        borderWidth: 2
    },
    iconFilterStyle: {
        width: 50,
        height: 50
    },
    //card props
    cardViewContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    iconStyle: {
        width: 110,
        height: 110
    },
    cardViewCol: {
        flex: 1,
        alignItems: 'center',
        paddingHorizontal: 2
    },
    temperatureContent: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardCustomStyle: {
        backgroundColor: '#3ec1c9'
    }
})

export default CitiesWeatherScreen