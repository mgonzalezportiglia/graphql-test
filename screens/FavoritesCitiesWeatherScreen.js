import React from 'react'
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity } from 'react-native'
import { useStore } from '../store/storeHook'
import Card from '../components/Card'
import Icon from 'react-native-vector-icons/FontAwesome';
import { addToFavHandler, isFavorite } from '../utils/commonMedhods'

const FavoritesCitiesWeatherScreen = props => {

    const [store, setStore] = useStore()

    const onAddToFav = (id: string) => {
        const updatedStore = addToFavHandler(id, store)
        setStore('SET_FAVORITES', updatedStore)
    }

    const renderGridItem = (itemData) => {
        const { weather } = itemData.item

        return <Card
            style={styles.cardCustomStyle}>

            <View style={styles.cardViewContainer}>
                <View>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#0e0e0e' }}>{itemData.item.name}</Text>
                    <Image style={styles.iconStyle} source={{ uri: `http://openweathermap.org/img/wn/${weather.summary.icon}@2x.png` }} />
                </View>
                <View>
                    <TouchableOpacity style={{ top: 50 }} onPress={onAddToFav.bind(this, itemData.item.id)}>
                        <View>
                            <Icon
                                name="bookmark"
                                size={25}
                                color={isFavorite(itemData.item.id, store.favorites) ? '#fda82f' : '#fff'}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>

        </Card>
    }

    return (

        <View style={styles.container}>
            <View>
                {
                    store.favorites.length > 0 ?
                        <FlatList
                            keyExtractor={(item, index) => item.id}
                            numColumns={2}
                            data={store.favorites}
                            renderItem={renderGridItem}
                        />

                        :
                        <View style={{top: 100, alignItems: 'center'}}>
                            <Text style={{
                                fontSize: 18,
                                fontWeight: 'bold',
                                color: '#ec6e4c'
                            }}>You haven't favorite cities.</Text>
                        </View>
                }
            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 60
    },
    //card props
    cardViewContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    iconStyle: {
        width: 110,
        height: 110
    },
    temperatureContent: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardCustomStyle: {
        backgroundColor: '#3ec1c9'
    }
})

export default FavoritesCitiesWeatherScreen