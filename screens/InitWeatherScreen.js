import React from 'react'
import { View, Text, Button, StyleSheet, Image, Animated, Easing, TouchableOpacity } from 'react-native'
import { useStore } from '../store/storeHook'
import { citiesArray } from '../src/constants'
import { gql, useQuery } from '@apollo/client'
import configureDataStore from '../store/dataStore'
import styled from 'styled-components/native';

const Container = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
    background-color: #3ec1c9;
`;

const TextStyled = styled.Text`
    font-size: 18px;
    font-weight: bold;
    color: #fcfefe;
    width: 200px;
    text-align: center;
    margin-top: 150px;
`

configureDataStore()

const GET_CITIES_BY_ID_QUERY = gql`
    query($idCity: [String!]) {
        getCityById(id: $idCity) {
            id
            name
            weather {
                summary {
                    icon
                    description
                }
                temperature {
                    actual
                    min
                    max
                }
            }
            coord {
                lon
                lat
            }
        }
    }
`
const InitWeatherScreen = props => {

    const spinLogo = new Animated.Value(0)

    Animated.loop(
        Animated.timing(
            spinLogo,
            {
                toValue: 1,
                duration: 10000,
                easing: Easing.linear,
                useNativeDriver: true
            }
        )
    ).start()

    const spin = spinLogo.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
    })

    const [store, setStore] = useStore()

    // cuando react se monta y renderiza este componente llama a esta funcion.
    const { data, loading } = useQuery(GET_CITIES_BY_ID_QUERY, {
        variables: {
            idCity: citiesArray
        },
        onCompleted: () => {
            const cities = data.getCityById
            setStore('SET_CITIES', cities)
        }
    })

    if (loading) {
        return (
            <Container>
                <TextStyled>Loading...</TextStyled>
            </Container>
        )
    }

    return (
        <Container>
            <TouchableOpacity
                onPress={() => {
                    props.navigation.navigate('WeatherBottomTabs') // CitiesWeather
                }}
            >
                <Animated.Image
                    source={require('../assets/sun.png')}
                    style={{ transform: [{ rotate: spin }], width: 250, height: 250 }}
                />
            </TouchableOpacity>
            <TextStyled>Press the sun to enter the app.</TextStyled>
        </Container>
    )
}


export default InitWeatherScreen