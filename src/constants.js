// SCREENS
import InitWeatherScreen from '../screens/InitWeatherScreen'
import CitiesWeatherScreen from '../screens/CitiesWeatherScreen'
import CityWeatherDetailScreen from '../screens/CityWeatherDetailScreen'
import favoritesCitiesWeatherScreen from '../screens/FavoritesCitiesWeatherScreen'
import FavoritesCitiesWeatherScreen from '../screens/FavoritesCitiesWeatherScreen';
// MODALS
import SearchCityModalScreen from '../screens/modal/SearchCityModalScreen'

//DATA

export const citiesArray = [
  '3435910',
  '2988507',
  '292223',
  '2147714',
  '1850144',
  '1261481',
  '3451190',
  '2643743',
];


// UTILS

export const colors = {
  primaryColor: '#4a148c',
  accentColor: '#ff6f00',
  celesteColor: '#3ec1c9',
  whiteColor: '#fcfefe'
}

export const kelvin = 273.15

// BREADCRUMB

export const breadcrumbScreen = {
  //SCREENS
  initWeather: {
    name: "InitWeather", title: "Weather App", component: InitWeatherScreen
  },
  citiesWeather: {
    name: "CitiesWeather", title: "Cities Weather", component: CitiesWeatherScreen
  },
  cityWeatherDetail: {
    name: "CityWeatherDetail", title: "City Weather Detail", component: CityWeatherDetailScreen
  },
  favoritesCitiesWeather: {
    name: "FavoritesCitiesWeather", title: "Favorites Cities Weather", component: FavoritesCitiesWeatherScreen
  },
  searchCity: {
    name: "SearchCityModal", title: "Search city modal screen", component: SearchCityModalScreen
  }
}