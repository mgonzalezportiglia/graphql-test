import { initStore } from './storeHook'

const configureDataStore = () => {
    const actions = {
        SET_CITIES: (state, cities) => ({ ...state, cities }),
        SET_FAVORITES: (state, favorites) => ({ ...state, favorites })
    }

    initStore(actions, {
        cities: [],
        favorites: []
    })
}

export default configureDataStore;