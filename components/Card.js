import React from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Image } from 'react-native';

const Card = props => {

    return (
        <TouchableWithoutFeedback onPress={props.navigation}>
            <View style={{ ...style.mainCardView, ...props.style }}>
                {props.children}
            </View>
        </TouchableWithoutFeedback>
    )
}

const style = StyleSheet.create({
    mainCardView: {
        height: 150,
        justifyContent: 'center',
        backgroundColor: '#f9f9f9',
        borderRadius: 15,
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowOffset: { x: 0, y: 10 },
        shadowOpacity: 1,
        shadowRadius: 8,
        elevation: 8,
        paddingLeft: 16,
        paddingRight: 14,
        marginTop: 6,
        marginBottom: 6,
        marginLeft: 16,
        marginRight: 16
    }
});

export default Card;