import * as React from 'react';
import { Text, View, StyleSheet, AppRegistry } from 'react-native';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';

import WeatherAppNavigator from './navigation/WeatherAppNavigator'

const LOCALITIES_CLIENT_GRAPHQL = 'https://graphql-weather-api.herokuapp.com/'

const client = new ApolloClient({
  uri: LOCALITIES_CLIENT_GRAPHQL,
  cache: new InMemoryCache()
});

const App = () => {
  return (<ApolloProvider client={client}>
    <WeatherAppNavigator />
  </ApolloProvider>
  )
}

AppRegistry.registerComponent('WeatherApplication', () => App)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',

  }
})

export default App;