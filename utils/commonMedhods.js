export const KELVIN = 273.15

export const addToFavHandler = (id: string, store: any) => {
    let updatedFavoritesStore = []
    const isAlreadyFavorite = store.favorites.findIndex((city) => city.id === id)
    if (isAlreadyFavorite !== -1) {
        return store.favorites.filter((fav, index) => index !== isAlreadyFavorite)

    }
    updatedFavoritesStore = store.cities.find((city) => city.id === id)
    return [...store.favorites, updatedFavoritesStore]
}

export const isFavorite = (id: string, favorites: any[]) => {
    const isAlreadyFav = favorites.findIndex((fav) => fav.id === id)
    return isAlreadyFav !== -1
}

export const formatTemperature = (temperature: number) => {
    return Math.round(temperature - KELVIN) + "°"
}